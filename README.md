This is fork of Edwin's LogitechFFDrivers 
http://steamcommunity.com/groups/linuxff/discussions/0/405692224235574471/?ctp=8


> Edwin; 21 Feb, 2016 @ 5:31pm 
> 
> Driver Package for Logitech Wheels
> 
> Today I took some time to provide a "least effort" method to build the new and experimental drivers for all Logitech force feedback wheels. Since I personally don't really like distributing Linux kernel modules, I created a source archive that should build with a single command. The advantage of this is that it should work on most Linux distributions. However, if you try it on kernels before 4.2, some changes to the code may be needed. There is also at least one change needed for later kernels, but I'll deal with that if it is needed.
> 
> You can download the drivers here:
> 
> https://www.dropbox.com/s/n4bmw631d4t1m0t/LogitechFFDrivers.zip?dl=0
> 
> 
> Read the [README](README) carefully and report any problems you may have.
> 
> 
> Update 29 Feb 2016:  
> - Added ff-memless to modules load  
> - Added conditional changes for kernel 4.4 and later  
> 
> Update 22 Aug 2016:  
> - Changed Makefile to remove the hid-logitech module again after loading usbhid loads the wrong one.  
> - Fixed bug causing the wheel to turn right on Intertia/Friction effects for all wheels except G920.  
> 
> Last edited by Edwin; 22 Aug, 2016 @ 10:31pm	


